Name:           python-straight-plugin
Version:        1.5.0
Release:        2
Summary:        Python plugin loader

License:        MIT
URL:            https://github.com/ironfroggy/straight.plugin/
# Version 1.5.0 was released on pypi but not tagged on github
# Use source from pypi
Source0:        https://files.pythonhosted.org/packages/48/89/34ae6a87784d0b607af61c84a52c313c598f1d86ce5c1e9eb6da038fee5f/straight.plugin-1.5.0.tar.gz

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  /usr/bin/2to3
BuildRequires:  python3-setuptools

%description
straight.plugin is a Python plugin loader inspired by twisted.plugin.
The system is used to allow multiple Python packages to provide plugins within
a namespace package, where other packages will locate and utilize. The plugins
themselves are modules in a namespace package where the namespace identifies
the plugins in it for some particular purpose or intent.

%package -n     python3-straight-plugin
Summary:        Python plugin loader

%description -n python3-straight-plugin
straight.plugin is a Python plugin loader inspired by twisted.plugin.
The system is used to allow multiple Python packages to provide plugins within
a namespace package, where other packages will locate and utilize. The plugins
themselves are modules in a namespace package where the namespace identifies
the plugins in it for some particular purpose or intent.

%prep
%setup -q -n straight.plugin-%{version}
2to3 --write --nobackups .

%build
%py3_build

%install
%py3_install

%check
%{__python3} tests.py

%files -n python3-straight-plugin
%{python3_sitelib}/straight*

%changelog
* Tue May 10 2022 houyingchao <houyingchao@h-partners.com> - 1.5.0-2
- License compliance rectification

* Tue Jul 13 2021 wangdi <wangdi@kylinos.cn> - 1.5.0-1
- Package init

